package exception;

import java.io.*;
import java.util.Scanner;

public class CheckedExceptionDemo {

    public static void main(String[] args) {
    /*
        File file = new File(("D:\\data.txt"));
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(file);
            final char read = (char)fileReader.read();
            System.out.println(read);
        } catch (FileNotFoundException e) {
            System.out.println(" File is not present :: "+ e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(" Resource is busy :: "+ e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if( fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    */
        File file = new File(("D:\\data.txt"));
        //try with resources
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
             Scanner scanner = new Scanner(System.in)) {
            boolean flag = true;
            while( flag ){
                final String line = bufferedReader.readLine();
                if (line == null){
                    flag = false;
                    continue;
                }
                System.out.println(line);
            }
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        } catch (IOException | ArithmeticException | ArrayIndexOutOfBoundsException exception) {
            exception.printStackTrace();
        }
    }
}