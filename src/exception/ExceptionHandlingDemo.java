package exception;

public class ExceptionHandlingDemo {

    public static void main(String[] args) {
        int a = 45;
        int b = 10;
        int[] array = {11,22, 33, 44};

        try {
            System.out.println("The result is " + a / b);
            System.out.println(" Element at index 4 is "+ array[4]);
        }
        catch (ArithmeticException exception) {
            //handle the exception
            System.out.println("A number cannot be divided by zero:: ");
        } catch (ArrayIndexOutOfBoundsException exception) {
            System.out.println("Tryign to accesss the index from array");
        }

        System.out.println("This statement will not be printed");
    }
}